import React, {Component} from 'react';
import './App.css';
import Circle from "./components/Circle/Circle";

class App extends Component {
    state = {
        myNumbers: []
    };

    generateNumber = () => {
        const min = 5;
        const max = 36;
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    createNewNumbers = () => {
        const newNumbers = [];
        while (newNumbers.length < 5) {
            const newNumber = this.generateNumber();
            if (newNumbers.includes(newNumber)) {
                continue;
            } else {
                newNumbers.push(newNumber);
            }
        }
        newNumbers.sort((a, b) => {
            return a - b;
        })
        this.setState({myNumbers: newNumbers})
    }

    render() {
        const circles = this.state.myNumbers.map(num => {
            return <Circle num={num} key={this.state.myNumbers.indexOf(num)}/>
        })
        return (
            <div className="App">
                <h1>Lottery</h1>
                 {circles}
                <div>
                    <button onClick={this.createNewNumbers} className="btn">New numbers</button>
                </div>
            </div>
        );
    }
}

export default App;